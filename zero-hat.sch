EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 5A524BD5
P 5700 3200
F 0 "J1" H 5750 4200 50  0000 C CNN
F 1 "PinHeader_2x20_P2.54mm_Vertical" H 5750 2100 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 5700 3200 50  0001 C CNN
F 3 "" H 5700 3200 50  0001 C CNN
	1    5700 3200
	1    0    0    -1  
$EndComp
Text Label 6000 2300 0    60   ~ 0
5V
Text Label 6000 2400 0    60   ~ 0
5V
Text Label 6000 2500 0    60   ~ 0
GND
Text Label 6000 2900 0    60   ~ 0
GND
Text Label 6000 3200 0    60   ~ 0
GND
Text Label 6000 3700 0    60   ~ 0
GND
Text Label 6000 3900 0    60   ~ 0
GND
Text Label 5500 4200 2    60   ~ 0
GND
Text Label 5500 3500 2    60   ~ 0
GND
Text Label 5500 2700 2    60   ~ 0
GND
Text Label 5500 2300 2    60   ~ 0
3V3
Text Label 5500 3100 2    60   ~ 0
3V3
Text Label 6000 2600 0    60   ~ 0
BCM14
Text Label 6000 2700 0    60   ~ 0
BCM15
Text Label 6000 2800 0    60   ~ 0
BCM18
Text Label 6000 3000 0    60   ~ 0
BCM23
Text Label 6000 3100 0    60   ~ 0
BCM24
Text Label 6000 3300 0    60   ~ 0
BCM25
Text Label 6000 3400 0    60   ~ 0
BCM8
Text Label 6000 3500 0    60   ~ 0
BCM7
Text Label 6000 3600 0    60   ~ 0
BCM1
Text Label 6000 3800 0    60   ~ 0
BCM12
Text Label 6000 4000 0    60   ~ 0
BCM16
Text Label 6000 4100 0    60   ~ 0
BCM20
Text Label 6000 4200 0    60   ~ 0
BCM21
Text Label 5500 2400 2    60   ~ 0
BCM2
Text Label 5500 2500 2    60   ~ 0
BCM3
Text Label 5500 2600 2    60   ~ 0
BCM4
Text Label 5500 2800 2    60   ~ 0
BCM17
Text Label 5500 2900 2    60   ~ 0
BCM27
Text Label 5500 3000 2    60   ~ 0
BCM22
Text Label 5500 3200 2    60   ~ 0
BCM10
Text Label 5500 3300 2    60   ~ 0
BCM9
Text Label 5500 3400 2    60   ~ 0
BCM11
Text Label 5500 3600 2    60   ~ 0
BCM0
Text Label 5500 3700 2    60   ~ 0
BCM5
Text Label 5500 3800 2    60   ~ 0
BCM6
Text Label 5500 3900 2    60   ~ 0
BCM13
Text Label 5500 4000 2    60   ~ 0
BCM19
Text Label 5500 4100 2    60   ~ 0
BCM26
$Comp
L Mechanical:MountingHole H1
U 1 1 600828FF
P 9850 4800
F 0 "H1" H 9950 4846 50  0000 L CNN
F 1 "MountingHole" H 9950 4755 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 9850 4800 50  0001 C CNN
F 3 "~" H 9850 4800 50  0001 C CNN
	1    9850 4800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 60083C58
P 9850 5200
F 0 "H2" H 9950 5246 50  0000 L CNN
F 1 "MountingHole" H 9950 5155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 9850 5200 50  0001 C CNN
F 3 "~" H 9850 5200 50  0001 C CNN
	1    9850 5200
	1    0    0    -1  
$EndComp
$Comp
L SSD1306-128x64_OLED:SSD1306 SSD1306_128x64
U 1 1 60081617
P 4400 2850
F 0 "SSD1306_128x64" V 3950 2850 50  0000 C CNN
F 1 "SSD1306" V 4050 2850 50  0000 C CNN
F 2 "SSD1306:128x64OLED" H 4400 3100 50  0001 C CNN
F 3 "" H 4400 3100 50  0001 C CNN
	1    4400 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 2700 5500 2700
Wire Wire Line
	4750 2800 4800 2800
Wire Wire Line
	4800 2800 4800 2300
Wire Wire Line
	4800 2300 5500 2300
Wire Wire Line
	4750 2900 4850 2900
Wire Wire Line
	4850 2900 4850 2500
Wire Wire Line
	4850 2500 5500 2500
Wire Wire Line
	4750 3000 4900 3000
Wire Wire Line
	4900 3000 4900 2400
Wire Wire Line
	4900 2400 5500 2400
$Comp
L Device:LED D1
U 1 1 600819A0
P 6550 2600
F 0 "D1" H 6550 2750 50  0000 C CNN
F 1 "PWR" H 6550 2850 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 6550 2600 50  0001 C CNN
F 3 "~" H 6550 2600 50  0001 C CNN
	1    6550 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 600830C9
P 5000 3400
F 0 "D2" H 5000 3600 50  0000 C CNN
F 1 "ACT" H 5000 3500 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 5000 3400 50  0001 C CNN
F 3 "~" H 5000 3400 50  0001 C CNN
	1    5000 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60083B33
P 7000 2600
F 0 "R1" V 7100 2600 50  0000 C CNN
F 1 "330" V 7200 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 6930 2600 50  0001 C CNN
F 3 "~" H 7000 2600 50  0001 C CNN
	1    7000 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 600847AC
P 4550 3400
F 0 "R2" V 4343 3400 50  0000 C CNN
F 1 "330" V 4450 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4480 3400 50  0001 C CNN
F 3 "~" H 4550 3400 50  0001 C CNN
	1    4550 3400
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push_Dual_x2 SW1
U 1 1 6008231B
P 4350 3950
F 0 "SW1" H 4250 4250 50  0000 L CNN
F 1 "SW_Push_Dual_x2" H 4050 4150 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 4350 4150 50  0001 C CNN
F 3 "~" H 4350 4150 50  0001 C CNN
	1    4350 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3950 4550 4100
Wire Wire Line
	4550 4100 5500 4100
Wire Wire Line
	4150 3950 4150 4200
Wire Wire Line
	4150 4200 5500 4200
Wire Wire Line
	5500 3400 5150 3400
Wire Wire Line
	4850 3400 4700 3400
Wire Wire Line
	4400 3400 4300 3400
Wire Wire Line
	4300 3400 4300 3500
Wire Wire Line
	4300 3500 5500 3500
Wire Wire Line
	6000 2600 6400 2600
Wire Wire Line
	6700 2600 6850 2600
Wire Wire Line
	7150 2600 7250 2600
Wire Wire Line
	7250 2600 7250 2500
Wire Wire Line
	7250 2500 6000 2500
$EndSCHEMATC
